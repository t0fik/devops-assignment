# Peakdata DevOps Assignment
![](https://peakdata.com/wp-content/themes/healthscape/images/logo.svg)

## Introduction
This repository holds the Peakdata DevOps assignment. 
In this project you will find a folder that holds a microservice, supplied with a Dockerfile.

### Web Server
Web Server is a simple stateless service that serves a simple HTTP message. 
You can deploy as many replicas of this service as you like.

## The assignment
You as our devops expert, tell us that we need a proper CI/CD!
Since we use GitLab, this can be implemented with a GitLab runner. 
Please put this files into a repo on your personal gitlab account.
And set up a gitlab [CI/CD](https://docs.gitlab.com/ee/ci/). 

BEFORE YOU START:
- Check in this project into your personal gitlab account.
- Create a feature branch for your changes.
- Make sure you walk us though all your steps by describing them to the
assignment_notes.md file.

There are a couple of requirements:
- [ ] Test should be run on the pipeline and it should not be able to proceed if it fails.
- [ ] The app should be deployed to a Kubernetes cluster (take a look at Minikube).
- [ ] Multiple feature branches should be able to live alongside each other in the cluster.

Some pointers:
- NodeJS apps manage their dependencies in a package.json file, `npm install` installs the dependencies in a folder called `node_modules`.
- The tests can be executed by running `npm test`.

FINISHING UP:
Once you are finished please create a merge request from your feature branch and share the merge request link with us. 

Good luck with the assignment!

### Optional additions
- [ ] Gather metrics from deployed application
- [ ] Alerting in-case of anomalies in application


